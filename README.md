# Sainsbury's Tech Test

## Building

Execute
```bash
mvn clean package
```

## Testing

Execute
```bash
mvn test
```

## Run

Execute
```bash
java -jar target/sainsburys-tech-test-1.0-SNAPSHOT-jar-with-dependencies.jar
```

##Libraries

- Jsoup - Parsing and scanning of HTML
- Jsonb - Marshalling/ Unmarshalling of JSON
- Lombok - Replaces Getters/ Setters and Constructors with Annotations