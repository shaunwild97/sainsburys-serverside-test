package com.sainsburys.techtest.services;

import com.sainsburys.techtest.data.Product;
import com.sainsburys.techtest.data.ProductsResponse;

public interface SainsburysService {
    public ProductsResponse getProducts(String url);

    public Product getProduct(String url);
}
