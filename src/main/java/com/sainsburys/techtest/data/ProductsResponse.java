package com.sainsburys.techtest.data;

import lombok.Data;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductsResponse {
    private List<Product> results;

    public BigDecimal getTotal() {
        return results.stream()
                .map(p -> p.getUnitPrice())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
